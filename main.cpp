#include <iostream>

#include "CodeAnalysis/Syntax/include/lexer.h"

int main() {
    while (true) {
        std::cout << "> ";
        std::string line;
        std::getline(std::cin, line);

        if (line.empty())
            break;

        kavarin::code::syntax::lexer lexer (line);

        while (true) {
            kavarin::code::syntax::syntax_token token = lexer.lex();

            if (token.kind == kavarin::code::syntax::EndOfFileToken)
                break;

            std::cout << skts(token.kind) << ": '" << token.text << "'";

            if (!isnull(token.value))
                std::cout << " " << std::any_cast<int>(token.value);

            std::cout << std::endl;
        }
    }

    return 0;
}
