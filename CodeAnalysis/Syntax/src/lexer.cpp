#include "../include/lexer.h"

#define OPERATOR_TOKEN_CASE(chare, token_type) case #@chare: \
return syntax_token(token_type, position++, #chare, nullptr);

namespace kavarin::code::syntax {
    lexer::lexer(std::string source) {
        this->source = source;
        this->position = 0;
    }

    char lexer::get_current() {
        if (position >= source.length())
            return '\0';

        return source[position];
    }

    void lexer::next() {
        position++;
    }

    syntax_token lexer::lex() {
        if (position >= source.length())
            return syntax_token(EndOfFileToken, position, "\0", nullptr);

        if (isdigit(get_current())) {
            int start = position;

            while (isdigit(get_current()))
                next();

            int length = position - start;
            std::string text = source.substr(start, length);

            return syntax_token(LiteralToken, position, text, string2int(text));
        }

        if (isblank(get_current())) {
            int start = position;

            while (isblank(get_current()))
                next();

            int length = position - start;
            std::string text = source.substr(start, length);
            return syntax_token(WhitespaceToken, position, text, nullptr);
        }

        switch (get_current())
        {
            OPERATOR_TOKEN_CASE(  +  ,  PlusToken     )
            OPERATOR_TOKEN_CASE(  -  ,  MinusToken    )
            OPERATOR_TOKEN_CASE(  /  ,  SlashToken    )
            OPERATOR_TOKEN_CASE(  *  ,  StarToken     )
            OPERATOR_TOKEN_CASE(  ^  ,  CarrotToken   )
            OPERATOR_TOKEN_CASE(  %  ,  PercentToken  )
        }

        return syntax_token(BadToken, position++, source.substr(position - 1, 1), nullptr);
    }
}