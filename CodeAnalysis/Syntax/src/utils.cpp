#include "../include/utils.h"

#define CASE_KIND(kind) case kind: \
return #kind;

using namespace kavarin::code::syntax;

int string2int(std::string s) {
    std::stringstream ss (s);
    int n = 0;
    ss >> n;
    return n;
}

bool isnull(std::any ref) {
    try {
        std::any_cast<nullptr_t>(ref);
        return true;
    } catch (std::bad_any_cast) {
        return false;
    }
}

std::string skts(syntax_kind kind) {
    switch (kind) {
        CASE_KIND(kavarin::code::syntax::PlusToken)
        CASE_KIND(kavarin::code::syntax::MinusToken)
        CASE_KIND(kavarin::code::syntax::SlashToken)
        CASE_KIND(kavarin::code::syntax::StarToken)
        CASE_KIND(kavarin::code::syntax::CarrotToken)
        CASE_KIND(kavarin::code::syntax::PercentToken)
        CASE_KIND(kavarin::code::syntax::OpenParenthesisToken)
        CASE_KIND(kavarin::code::syntax::CloseParenthesisToken)
        CASE_KIND(kavarin::code::syntax::LiteralToken)
        CASE_KIND(kavarin::code::syntax::WhitespaceToken)
        CASE_KIND(kavarin::code::syntax::EndOfFileToken)
        CASE_KIND(kavarin::code::syntax::EmptyToken)
        CASE_KIND(kavarin::code::syntax::BadToken)
    }
}