#include "../include/syntax_token.h"

namespace kavarin::code::syntax {
    syntax_token::syntax_token(syntax_kind kind, int position, std::string text, std::any value) {
        this->kind = kind;
        this->position = position;
        this->text = text;
        this->value = value;
    }

    syntax_token::syntax_token() {
        this->kind = EmptyToken;
        this->position = 0;
        this->text = "";
        this->value = nullptr;
    }
}