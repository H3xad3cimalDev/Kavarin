#ifndef KAVARIN_SYNTAX_TOKEN_H
#define KAVARIN_SYNTAX_TOKEN_H

#include <iostream>
#include <string>
#include <any>

#include "syntax_kind.h"

namespace kavarin::code::syntax {
    class syntax_token {
    public:
        std::string text;
        int position;
        syntax_kind kind;
        std::any value;

        syntax_token(syntax_kind kind, int position, std::string text, std::any value);
        syntax_token();
    };
}


#endif //KAVARIN_SYNTAX_TOKEN_H
