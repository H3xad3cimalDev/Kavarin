#ifndef KAVARIN_LEXER_H
#define KAVARIN_LEXER_H

#include <iostream>
#include <string>
#include <vector>

#include "syntax_token.h"
#include "syntax_kind.h"
#include "utils.h"

namespace kavarin::code::syntax {
    class lexer {
    private:
        std::string source;
        int position;

        void next();
        char get_current();
    public:
        lexer(std::string source);

        syntax_token lex();
        std::vector<syntax_token> lex_all();
    };
}

#endif //KAVARIN_LEXER_H