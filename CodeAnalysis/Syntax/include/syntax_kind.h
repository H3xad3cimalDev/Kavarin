#ifndef KAVARIN_SYNTAX_KIND_H
#define KAVARIN_SYNTAX_KIND_H

namespace kavarin::code::syntax {
    enum syntax_kind {
        PlusToken,
        MinusToken,
        SlashToken,
        StarToken,
        CarrotToken,
        PercentToken,

        OpenParenthesisToken,
        CloseParenthesisToken,

        LiteralToken,
        WhitespaceToken,

        EndOfFileToken,
        EmptyToken,
        BadToken
    };
}

#endif //KAVARIN_SYNTAX_KIND_H
