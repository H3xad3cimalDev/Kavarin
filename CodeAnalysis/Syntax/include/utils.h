#ifndef KAVARIN_UTILS_H
#define KAVARIN_UTILS_H

#include <iostream>
#include <sstream>
#include <string>
#include <any>

#include "syntax_kind.h"

int string2int(std::string s);
bool isnull(std::any ref);
std::string skts(kavarin::code::syntax::syntax_kind kind);

#endif //KAVARIN_UTILS_H
